class User < ActiveRecord::Base
  EMAIL_FORMAT = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates_format_of :email, with: EMAIL_FORMAT
  validates_uniqueness_of :email
  validates_presence_of :password
  validates_confirmation_of :password
end
