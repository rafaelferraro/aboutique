class Category < ActiveRecord::Base
  validates_presence_of :name

  has_many :sub_categories
end
