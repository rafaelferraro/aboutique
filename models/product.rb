class Product < ActiveRecord::Base
  belongs_to :sub_category

  validates_presence_of :title, :size, :color, :sale_price, :purchase_price, :sub_category
end
