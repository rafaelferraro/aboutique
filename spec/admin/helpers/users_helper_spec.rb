require 'spec_helper'

RSpec.describe "ABoutique::Admin::UsersHelper" do
  it 'includes authentication helper' do
    expect(ABoutique::Admin::UsersHelper.include? ABoutique::Admin::AuthenticationHelper).to be_truthy
  end
end
