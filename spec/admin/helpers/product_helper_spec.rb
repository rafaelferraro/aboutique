require 'spec_helper'

describe "ABoutique::Admin::ProductHelper" do
  describe ".success_redirect" do
    let(:helper_class){ Class.new }
    before { helper_class.extend ABoutique::Admin::ProductHelper }
    subject { helper_class }
    let(:flash) { Hash[notice: ""] }

    it "redirects to any url" do
      allow(subject).to receive(:flash).and_return(flash)

      expect(subject).to receive(:redirect).with("url_for")
      subject.success_redirect("url_for", "model.product.success.save")
    end
  end
end
