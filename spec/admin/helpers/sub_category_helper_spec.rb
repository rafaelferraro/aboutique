require 'spec_helper'

describe "ABoutique::Admin::SubCategoryHelper" do
  let(:helper_class) { Class.new }

  before { helper_class.extend ABoutique::Admin::SubCategoryHelper }

  describe "success_redirect" do
    let(:flash) { Hash[notice: ""] }

    it "redirects to any url" do
      allow(helper_class).to receive(:flash).and_return(flash)

      expect(helper_class).to receive(:redirect).with("url_for")
      helper_class.success_redirect("url_for", "model.category.success.save")
    end
  end
end
