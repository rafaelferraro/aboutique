require 'spec_helper'

RSpec.describe "ABoutique::Admin::AdminHelper" do
  it "includes authentication helper" do
    expect(ABoutique::Admin::AdminHelper.include? ABoutique::Admin::AuthenticationHelper).to be_truthy
  end
end
