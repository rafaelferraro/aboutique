require 'spec_helper'

RSpec.describe "/admin" do
  describe 'GET /admin' do
    let(:url) { '/admin' }
    let(:login_url) { '/admin/users/login' }

    context 'when request is from login page' do
      before do
        allow_any_instance_of(ABoutique::Admin).to receive(:from_login?){ true }
      end

      it 'creates the session and cookie' do
        expect_any_instance_of(ABoutique::Admin).to receive(:create_session_and_cookie)
        get url
      end
    end

    context 'when request came from anywhere less from login page' do
      context 'when user is logged' do
        
        before do
          allow_any_instance_of(ABoutique::Admin).to receive(:from_login?){ false }
          allow_any_instance_of(ABoutique::Admin).to receive(:authenticated?){ true }
        end

        it 'follows the flow' do
          get url
          expect(last_response.status).to eq(200)
        end
      end

      context 'when user is not logged' do
        it "redirects the user to login's page" do
          get url
          expect(last_response['Location']).to match(login_url)
        end
      end
    end
  end
end
