require 'spec_helper'

describe "GET /users/login" do
  let(:url) { '/admin/users/login' }
  it 'returns a 200 status code' do
    get url
    expect(last_response.status).to eq(200)
  end
end

describe "POST /users/login" do
  let(:user)  { User.create(email: "ferraromv@gmail.com", password: "rspectest") }
  let(:url)   { "/admin/users/login" }

  it 'finds the user that has the same credentials like the params' do
    expect(User).to receive(:where).with({ email: "ferraromv@gmail.com", password: "rspectest" }).and_return([])
    post url, { user: { email: "ferraromv@gmail.com", password: "rspectest", password_confirmation: "rspectest" } }
  end

  context 'when the user is found' do
    let(:params) { Hash[user: Hash[email: user.email, password: user.password]] }

    it 'redirects the user to admin home page' do
      post url, params
      expect(last_response['Location']).to eq('http://example.org/admin')
    end

    it 'returns a 302 status code' do
      post url, params
      expect(last_response.status).to eq(302)
    end
  end

  context 'when the user is not found' do
    let(:params) { Hash[user: Hash[email: user.email, password: '123']] }

    it 'returns a 200 status code' do
      post url, params
      expect(last_response.status).to eq(200)
    end

    it 'shows a error message' do
      post url, params
      expect(last_response.body.include? I18n.t('models.user.login.invalid_user')).to be_truthy
    end
  end
end

describe "DELETE /users/logout" do
  let(:user)  { User.create(email: "ferraromv@gmail.com", password: "rspectest") }
  let(:login_url)   { "/admin/users/login" }
  let(:logout_url)  { "/admin/users/logout" }

  it 'destroys the logged session' do
    expect_any_instance_of(ABoutique::Admin).to receive(:destroy_session)
    delete logout_url
  end

  it 'redirects to login page' do
    delete logout_url
    expect(last_response['Location']).to match(login_url)
  end
end
