require 'spec_helper'

describe 'when user is logged' do
  before do
    allow_any_instance_of(ABoutique::Admin).to receive(:authenticated?){ true }
  end

  describe "GET /admin/sub_categories" do
    let(:url) { "/admin/sub_categories" }

    it "returns 200 as status code" do
      get url
      expect(last_response.status).to eq(200)
    end

    it 'catchs all sub categories on database' do
      expect(SubCategory).to receive(:all).and_return([])
      get url
    end
  end

  describe "GET /admin/sub_categories/new" do
    let(:url) { "/admin/sub_categories/new" }

    it "returns a 200 status code" do
      get url
      expect(last_response.status).to eq(200)
    end

    it 'builds a new instance of SubCategory' do
      expect(SubCategory).to receive(:new).and_return(SubCategory.new)
      get url
    end

    it 'gets a list of categories' do
      expect(Category).to receive(:all).and_return([])
      get url
    end
  end

  describe "POST /admin/sub_categories/create" do
    let(:category) { Category.create(name: 'category') }
    let(:sub_category) { SubCategory.new }
    let(:params) { Hash[sub_category: Hash[name: "category_name", description: "description", category_id: category.id]] }
    let(:url)    { "/admin/sub_categories/create" }

    it 'creates a new instance of SubCategory with correct params' do
      expect(SubCategory).to receive(:new).with({ "name" => "category_name", "description" => "description", "category_id" => category.id.to_s }).and_return(sub_category)
      post url, params
    end

    it 'tries to save a new instance of SubCategory' do
      allow(SubCategory).to receive(:new).and_return(sub_category)

      expect(sub_category).to receive(:save)
      post url, params
    end

    context "when SubCategory's instance wasnt persisted" do
      before do
        allow(SubCategory).to receive(:new).and_return(sub_category)
        allow(sub_category).to receive(:save).and_return(false)
      end

      it 'returns a 200 status code' do
        post url, params
        expect(last_response.status).to eq(200)
      end

      it 'doesnt make a redirect' do
        post url, params
        expect(last_response['Location']).to be_nil
      end
    end

    context "when SubCategory's instance was persisted" do
      it 'returns a 302 status code' do
        post url, params
        expect(last_response.status).to eq(302)
      end

      it 'redirects to a sub_categories listing' do
        post url, params
        expect(last_response['Location']).to eq('http://example.org/admin/sub_categories')
      end
    end
  end

  describe "GET /admin/sub_categories/:id/edit" do
    let(:category) { Category.create(name: 'category') }
    let(:sub_category) { SubCategory.create(name: "sub_category", category_id: category.id) }
    let(:url) { "/admin/sub_categories/#{sub_category.id}/edit" }

    it 'returns a 200 status code' do
      get url
      expect(last_response.status).to eq(200)
    end

    it 'finds a sub_category from database' do
      expect(SubCategory).to receive(:find).with(sub_category.id.to_s).and_return(sub_category)
      get url
    end

  end

  describe "PUT /admin/sub_categories/:id/update" do
    let(:category) { Category.create(name: 'category') }
    let(:sub_category) { SubCategory.create(name: "sub_category_name", category_id: category.id) }
    let(:url)     { "/admin/sub_categories/#{sub_category.id}/update" }
    let(:params)  { Hash[sub_category: Hash[name: "sub_category_updated", category_id: category.id]] }

    it 'finds a sub_category from database' do
      expect(SubCategory).to receive(:find).with(sub_category.id.to_s).and_return(sub_category)
      put url, params
    end

    context 'when sub_category was updated' do

      it 'returns a 302 status code' do
        put url, params
        expect(last_response.status).to eq(302)
      end

      it 'redirects to sub_categories listing' do
        put url, params
        expect(last_response['Location']).to eq('http://example.org/admin/sub_categories')
      end
    end

    context 'when sub_category wasnt updated' do

      before do
        allow(SubCategory).to receive(:find).and_return(sub_category)
        allow(sub_category).to receive(:update).and_return(false)
      end

      it 'returns a 200 status code' do
        put url, params
        expect(last_response.status).to eq(200)
      end

      it 'doesnt redirects to anywhere else' do
        put url, params
        expect(last_response['Location']).to be_nil
      end
    end
  end

  describe "DELETE /admin/sub_categories/:id" do
    let(:category) { Category.create(name: 'category') }
    let(:sub_category) { SubCategory.create(name: "name", category_id: category.id) }
    let(:url) { "/admin/sub_categories/#{sub_category.id}" }

    it 'finds a instance of SubCategory' do
      expect(SubCategory).to receive(:find).with(sub_category.id.to_s).and_return(sub_category)
      delete url
    end

    it 'destrois a sub_category' do
      delete url
      expect(SubCategory.where(id: sub_category.id)).to be_empty
    end

    it 'redirects to sub_categories listing' do
      delete url
      expect(last_response['Location']).to eq('http://example.org/admin/sub_categories')
    end
  end
end

describe 'when user is not logged' do
  let(:login_url) { "/admin/users/login" }

  describe "GET /admin/sub_categories" do
    it 'redirects to logins page' do
      get '/admin/sub_categories'
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "GET /admin/sub_categories/new" do
    it 'redirects to logins page' do
      get '/admin/sub_categories/new'
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "POST /admin/sub_categories/create" do
    let(:params) { { sub_category: { name: 'sub_category', category_id: '1' } } }

    it 'redirects to logins page' do
      post '/admin/sub_categories/create', params
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "GET /admin/sub_categories/:id/edit" do
    let(:category)      { Category.create(name: 'category') }
    let(:sub_category)  { SubCategory.create(name: 'sub_category', category_id: category.id) }

    it 'redirects to logins page' do
      get "/admin/sub_categories/#{sub_category.id}/edit"
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "PUT /admin/sub_categories/:id/update" do
    let(:category)      { Category.create(name: 'category') }
    let(:sub_category)  { SubCategory.create(name: 'sub_category', category_id: category.id) }

    it 'redirects to logins page' do
      put "/admin/sub_categories/#{sub_category.id}/update", {}
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "DELETE /admin/sub_categories/:id" do
    let(:category)      { Category.create(name: 'category') }
    let(:sub_category)  { SubCategory.create(name: 'sub_category', category_id: category.id) }

    it 'redirects to logins page' do
      delete "/admin/sub_categories/#{sub_category.id}"
      expect(last_response['Location']).to match(login_url)
    end
  end
end