require 'spec_helper'

describe 'CategoriesControllers', type: :controller do
  describe 'when user is not logged' do
    let(:login_url) { '/admin/users/login' }

    before do
      allow_any_instance_of(ABoutique::Admin).to receive(:authenticated?){ false }
    end

    describe 'GET /admin/categories' do
      it '' do
        get '/admin/categories'
        expect(last_response['Location']).to match(login_url)
      end
    end

    describe 'GET /admin/categories/new' do
      it '' do
        get '/admin/categories/new'
        expect(last_response['Location']).to match(login_url)
      end
    end

    describe 'POST /admin/categories/create' do
      it '' do
        post '/admin/categories/create', {category: { name: "category"}}
        expect(last_response['Location']).to match(login_url)
      end
    end

    describe 'GET /admin/categories/:id/edit' do
      let(:category) { Category.create(name: 'category') }

      it '' do
        get "/admin/categories/#{category.id}/edit"
        expect(last_response['Location']).to match(login_url)
      end
    end

    describe 'PUT /admin/categories/:id/update' do
      let(:category) { Category.create(name: 'category') }

      it '' do
        put "/admin/categories/#{category.id}/update"
        expect(last_response['Location']).to match(login_url)
      end
    end

    describe 'DELETE /admin/categories/:id/destroy' do
      let(:category) { Category.create(name: 'category') }

      it '' do
        delete "/admin/categories/#{category.id}/destroy"
        expect(last_response['Location']).to match(login_url)
      end
    end
  end

  describe 'when the user is logged' do
    before do
      allow_any_instance_of(ABoutique::Admin).to receive(:authenticated?){ true }
    end

    describe '/admin/categories' do
      it 'returns a 200 status code' do
        response = get '/admin/categories'
        expect(response.status).to eq(200)
      end

      it 'gets all categories from the database' do
        expect(Category).to receive(:all).and_return([])
        get '/admin/categories'
      end
    end

    describe '/admin/categories/new' do
      let(:category) { Category.new }

      it 'creates a new instance of Category' do
        expect(Category).to receive(:new).and_return(category)
        get '/admin/categories/new'
      end

      it 'returns a 200 status code' do
        response = get '/admin/categories/new'
        expect(response.status).to eq(200)
      end
    end

    describe '/admin/categories/create' do
      let(:category) { Category.new(name: 'category') }

      it 'creates a new instance of Category' do
        expect(Category).to receive(:new).with({ "name" => 'category' }).and_return(category)
        post '/admin/categories/create', {category: { name: "category"}}
      end

      context 'when category is successfully persisted' do
        let(:params) { Hash[:category => Hash[name: "category_name", description: "category_description"]] }

        it 'redirects to categories list' do
          post '/admin/categories/create', params
          expect(last_response["Location"]).to eq('http://example.org/admin/categories')
        end

        it 'returns a 302 status code' do
          post '/admin/categories/create', params
          expect(last_response.status).to eq(302)
        end
      end

      context 'when category is not persisted' do
        let(:params) { Hash[:category => Hash[]] }

        it 'returns a 200 status code' do
          post '/admin/categories/create', params
          expect(last_response.status).to eq(200)
        end
      end
    end

    describe '/admin/categories/:id/edit' do
      let(:category) { Category.create(name: 'category') }
      let(:url) { "/admin/categories/#{category.id}/edit" }

      it 'finds a Category by id' do
        expect(Category).to receive(:find).with(category.id.to_s).and_return(category)
        get url
      end

      it 'returns a 200 status code' do
        get url
        expect(last_response.status).to eq(200)
      end
    end

    describe '/admin/categories/:id/update' do
      let(:category) { Category.create(name: 'category', description: 'description') }
      let(:url)      { "/admin/categories/#{category.id}/update" }

      before do
        allow(Category).to receive(:find).and_return(category)
      end

      context 'when category is successfully updated' do
        let(:params)   { Hash[id: category.id, category: Hash[name: 'updated name of category', description: 'updated description of category']] }

        it 'updates the category' do
          expect(category).to receive(:update).with({ "name" => 'updated name of category', "description" => 'updated description of category' })
          put url, params
        end

        it 'returns a 302 status code' do
          put url, params
          expect(last_response.status).to eq(302)
        end

        it 'redirects to categories listing' do
          put url, params
          expect(last_response['Location']).to eq('http://example.org/admin/categories')
        end

        it 'updates category attributes' do
          put url, params
          expect(category.name).to eq('updated name of category')
          expect(category.description).to eq('updated description of category')
        end
      end

      context 'when category is not updated' do
        let(:params) { Hash[id: category.id, category: Hash[name: nil]] }

        it 'returns a 200 statuse code' do
          put url, params
          expect(last_response.status).to eq(200)
        end
      end
    end

    describe '/admin/categories/:id/destroy' do
      let(:category) { Category.create(name: 'category name') }
      let(:url) { "/admin/categories/#{category.id}/destroy" }

      it 'finds the record by id' do
        expect(Category).to receive(:find).with(category.id.to_s).and_return(category)
        delete url
      end

      it 'destroys this category from database' do
        allow(Category).to receive(:find).and_return(category)

        expect(category).to receive(:destroy)
        delete url
      end

      it 'redirects to categories listing' do
        delete url
        expect(last_response['Location']).to eq('http://example.org/admin/categories')
      end
    end
  end
end