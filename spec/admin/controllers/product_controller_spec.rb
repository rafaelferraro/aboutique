require 'spec_helper'

describe "when user is logged" do
  before do
    allow_any_instance_of(ABoutique::Admin).to receive(:authenticated?){ true }
  end

  describe "/admin/products" do
    let(:category) { Category.create(name: 'category') }
    let(:sub_category) { SubCategory.create(name: 'sub_category', category: category) }

    describe 'GET /admin/products' do
      let(:url) { "/admin/products" }

      it 'returns a 200 status code' do
        get url
        expect(last_response.status).to eq(200)
      end

      it 'gets all Products on database' do
        expect(Product).to receive(:all).and_return([])
        get url
      end
    end

    describe 'GET /admin/products/new' do
      let(:url) { "/admin/products/new" }
      let(:product) { product = Product.new }

      it 'returns a 200 status code' do
        get url
        expect(last_response.status).to eq(200)
      end

      it 'creates a new instance of Product' do
        expect(Product).to receive(:new).and_return(product)
        get url
      end

      it 'gets all sub_categories on database' do
        expect(SubCategory).to receive(:all).and_return([])
        get url
      end
    end

    describe 'GET /admin/products/:id/edit' do
      let(:product) { Product.create(title: 'title', description: 'description', size: 'M', color: 'blue', purchase_price: 40.0, sale_price: 60.0, sub_category: sub_category) }
      let(:url) { "/admin/products/#{product.id}/edit" }

      it 'returns a 200 status code' do
        get url
        expect(last_response.status).to eq(200)
      end

      it 'finds a product from database' do
        expect(Product).to receive(:find).with(product.id.to_s).and_return(product)
        get url
      end
    end

    describe 'POST /admin/products/create' do
      let(:url) { "/admin/products/create" }

      context 'when product was persisted' do
        let(:params) { Hash[product: Hash[title: 'title', description: 'description', size: 'M', color: 'blue', purchase_price: 30, sale_price: 40, sub_category_id: sub_category.id]] }

        it 'returns 302 as status' do
          post url, params
          expect(last_response.status).to eq(302)
        end

        it 'redirects to products listing' do
          post url, params
          expect(last_response['Location']).to eq('http://example.org/admin/products')
        end

        it 'includes a new data on database' do
          post url, params
          expect(Product.all.count).to eq(1)
        end
      end

      context 'when product wasnt persisted' do
        let(:params) { Hash[product: Hash[]]}

        it 'returns 200 as status' do
          post url, params
          expect(last_response.status).to eq(200)
        end

        it 'doesnt include any data on database' do
          post url, params
          expect(Product.all.count).to eq(0)
        end
      end
    end

    describe 'PUT /admin/products/:id' do
      let(:product) { Product.create(title: 'title', description: 'description', size: 'M', color: 'blue', purchase_price: 40.0, sale_price: 60.0, sub_category: sub_category) }
      let(:url) { "/admin/products/#{product.id}" }

      context 'when product was updated' do
        let(:params) { Hash[product: Hash[title: 'updated_title']] }

        it 'returns 302 as status' do
          put url, params
          expect(last_response.status).to eq(302)
        end

        it 'redirects to products listing' do
          put url, params
          expect(last_response['Location']).to eq('http://example.org/admin/products')
        end

        it 'updates the attributes that has came from params' do
          put url, params
          expect(Product.find(product.id).title).to eq('updated_title')
        end
      end

      context 'when product wasnt updated' do
        let(:params) { Hash[product: Hash[title: nil]] }

        it 'returns 200 as status' do
          put url, params
          expect(last_response.status).to eq(200)
        end

        it 'doesnt redirects to anywhere else' do
          put url, params
          expect(last_response['Location']).to be_nil
        end
      end
    end

    describe 'DELETE /admin/products/:id' do
      let(:product) { Product.create(title: 'title', description: 'description', size: 'M', color: 'blue', purchase_price: 40.0, sale_price: 60.0, sub_category: sub_category) }
      let(:url) { "/admin/products/#{product.id}" }

      it 'destroys a product from database' do
        delete url
        expect { Product.find(product.id) }.to raise_error(ActiveRecord::RecordNotFound)
      end

      it 'redirects to products listing' do
        delete url
        expect(last_response['Location']).to eq('http://example.org/admin/products')
      end
    end
  end
end

describe "when user is not logged" do
  let(:login_url) { "/admin/users/login" }
  let(:category) { Category.create(name: 'category') }
  let(:sub_category) { SubCategory.create(name: 'sub_category', category: category) }
  let(:product) { Product.create(title: 'title', size: 'size', color: 'color', sale_price: 50.0, purchase_price: 30.0, sub_category: sub_category) }

  describe "GET /admin/products" do
    it 'redirects to logins page' do
      get "/admin/products"
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "GET /admin/products/new" do
    it 'redirects to logins page' do
      get "/admin/products/new"
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "GET /admin/products/:id/edit" do
    it 'redirects to logins page' do
      get "/admin/products/#{product.id}/edit"
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "POST /admin/products/create" do
    let(:params) { { product: {} } }

    it 'redirects to logins page' do
      post "/admin/products/create", params
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "PUT /admin/products/:id" do
    it 'redirects to logins page' do
      put "/admin/products/#{product.id}", {}
      expect(last_response['Location']).to match(login_url)
    end
  end

  describe "DELETE /admin/products/:id" do
    it 'redirects to logins page' do
      delete "/admin/products/#{product.id}"
      expect(last_response['Location']).to match(login_url)
    end
  end
end