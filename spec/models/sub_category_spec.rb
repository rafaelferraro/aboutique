require 'spec_helper'

describe SubCategory do
  let(:category) { Category.create(name: "category_name") }

  describe 'validations' do
    context 'when name is missing' do
      let(:sub_category) { described_class.new(description: "description", category: category) }

      it 'doesnt persist the sub_category' do
        expect(sub_category.save).to be_falsy
      end
    end

    context 'when category_id is missing' do
      let(:sub_category) { described_class.new(name: "name", description: "description") }

      it 'doesnt persist the sub_category' do
        expect(sub_category.save).to be_falsy
      end
    end

    context 'when description is missing' do
      let(:sub_category) { described_class.new(name: "name", category: category) }

      it 'persists the sub_category' do
        expect(sub_category.save).to be_truthy
      end
    end
  end

  describe 'belongs_to' do
    it 'responds to category method' do
      expect(described_class.new.respond_to? :category).to be_truthy
    end
  end
end