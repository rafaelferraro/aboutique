require 'spec_helper'

describe Product do
  it 'responds to sub_category' do
    expect(described_class.new.respond_to? :sub_category).to be_truthy 
  end

  describe 'validations' do
    let(:category) { Category.new(name: 'name') }
    let(:sub_category) { SubCategory.new(name: 'name', category: category) }
    let(:product) { Product.new(title: "title", description: "description", size: "M", color: "blue", purchase_price: 20.0, sale_price: 30.0, sub_category: sub_category) }

    context 'when title is missing' do
      before { product.title = nil }

      it 'doesnt persist the Product' do
        expect(product.save).to be_falsy
      end
    end

    context 'when description is missing' do
      before { product.description = nil }

      it 'persists the Product' do
        expect(product.save).to be_truthy
      end
    end

    context 'when size is missing' do
      before { product.size = nil }

      it 'doesnt persist the Product' do
        expect(product.save).to be_falsy
      end
    end

    context 'when color is missing' do
      before { product.color = nil }

      it 'doesnt persist the Product' do
        expect(product.save).to be_falsy
      end
    end

    context 'when purchase_price is missing' do
      before { product.purchase_price = nil }

      it 'doesnt persist the Product' do
        expect(product.save).to be_falsy
      end
    end

    context 'when sale_price is missing' do
      before { product.sale_price = nil }

      it 'doesnt persist the Product' do
        expect(product.save).to be_falsy
      end
    end

    context 'when sub_category is missing' do
      before { product.sub_category = nil }

      it 'doesnt persist the Product' do
        expect(product.save).to be_falsy
      end
    end
  end
end
