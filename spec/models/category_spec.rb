require 'spec_helper'

describe Category do
  describe '.validators' do
    context 'when name is missing' do
      let(:category) { described_class.new(description: 'description') }

      it 'doesnt persisted the category' do
        category.save
        expect(category.persisted?).to be_falsy
      end
    end

    context 'when description is missing' do
      let(:category) { described_class.new(name: 'name') }

      it 'persists the category' do
        category.save
        expect(category.persisted?).to be_truthy
      end
    end
  end
end
