require 'spec_helper'

describe User do
  let(:user) {
    User.new(
      email: "rspectest@gmail.com",
      password: "test12345",
      password_confirmation: "test12345"
    )
  }

  let(:user_copy) {
    User.new(
      email: "rspectest@gmail.com",
      password: "test",
      password_confirmation: "test"
    )
  }

  context 'when I try to save a user with an email that already exists' do
    before { user.save }

    it 'doesnt persist the user' do
      expect(user_copy.save).to be_falsy
    end
  end

  context 'when email is missing' do
    before { user.email = nil }

    it 'doesnt persist the user' do
      expect(user.save).to be_falsy
    end
  end

  context 'when email dont has a valid format' do
    before { user.email = 'test.com' }

    it 'doesnt persist the user' do
      expect(user.save).to be_falsy
    end
  end

  context 'when password is missing' do
    before { user.password = nil }

    it 'doesnt persist the user' do
      expect(user.save).to be_falsy
    end
  end
end
