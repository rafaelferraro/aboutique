class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.string :title
      t.string :description
      t.string :size
      t.string :color
      t.float  :sale_price
      t.float  :purchase_price
      t.integer :sub_category_id, index: true
      t.timestamps
    end
  end

  def self.down
    drop_table :products
  end
end
