ABoutique::Admin.controllers :admin do
  
  before :index do
    from_login? ? create_session_and_cookie : resources_authentication
  end

  get :index, :map => '/' do
    render '/admin/index'
  end

end
