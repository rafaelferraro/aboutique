ABoutique::Admin.controllers :users do
  get :login, :map => "/users/login" do
    render 'users/login'
  end

  post :login, :map => "/users/login" do
    @user = User.where(email: params[:user][:email], password: params[:user][:password]).first

    if @user
      redirect_to "/admin"
    else
      @message = I18n.t('models.user.login.invalid_user')
      render 'users/login'
    end
  end

  delete :logout, :map => "/users/logout" do
    destroy_session
    redirect_to "/admin/users/login"
  end
end
