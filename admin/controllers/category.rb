ABoutique::Admin.controllers :categories do

  before :index, :new, :create, :edit, :update, :destroy do
    resources_authentication
  end

  before :edit, :update, :destroy do
    @category = Category.find(params[:id])
  end

  get :index do
    @categories = Category.all
    render 'categories/index'
  end

  get :new do
    @category = Category.new
    render 'categories/new'
  end

  post :create do
    @category = Category.new(params[:category])

    if @category.save
      success_redirect(url(:categories, :index), 'models.category.success.save') # CategoryHelper
    else
      render 'categories/new'
    end
  end

  get :edit, :map => '/categories/:id/edit' do
    render 'categories/edit'
  end

  put :update, :map => '/categories/:id/update' do
    if @category.update(params[:category])
      success_redirect(url(:categories, :index), 'models.category.success.update') # CategoryHelper
    else
      render 'categories/edit'
    end
  end

  delete :destroy, :map => '/categories/:id/destroy' do
    @category.destroy

    success_redirect(url(:categories, :index), 'models.category.success.destroy') # CategoryHelper
  end
end
