ABoutique::Admin.controllers :products do

  before :index, :new, :create, :edit, :update, :show, :destroy do
    resources_authentication
  end

  before :edit, :update, :destroy do
    @product = Product.find(params[:id])
  end

  before :new, :create do
    @product = Product.new(params[:product])
  end

  get :index do
    @products = Product.all
    render 'products/index'
  end

  get :new do
    @sub_categories = SubCategory.all
    render 'products/new'
  end

  get :edit, :map => '/products/:id/edit' do
    @sub_categories = SubCategory.all
    render 'products/edit'
  end

  post :create, :map => '/products/create' do
    if @product.save
      success_redirect(url(:products, :index), "models.product.success.save")
    else
      render 'products/new'
    end
  end

  put :update, :map => '/products/:id' do
    if @product.update(params[:product])
      success_redirect(url(:products, :index), 'models.product.success.update')
    else
      render 'products/edit'
    end
  end

  delete :destroy, :map => '/products/:id' do
    @product.destroy

    success_redirect(url(:products, :index), 'models.product.success.destroy')
  end
end
