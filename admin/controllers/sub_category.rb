ABoutique::Admin.controllers :sub_categories do

  before :index, :new, :edit, :create, :update, :destroy do
    resources_authentication
  end

  before :edit, :update, :destroy do
    @sub_category = SubCategory.find(params[:id])
  end

  before :new, :create do
    attributes = params[:sub_category]

    @sub_category = SubCategory.new attributes
  end

  get :index do
    @sub_categories = SubCategory.all
    render 'sub_categories/index'
  end

  get :new do
    @categories = Category.all
    render 'sub_categories/new'
  end

  post :create do
    if @sub_category.save
      success_redirect(url(:sub_categories, :index), 'models.sub_category.success.save')
    else
      render 'sub_categories/new'
    end
  end

  get :edit, :map => "/sub_categories/:id/edit" do
    @categories = Category.all
    render 'sub_categories/edit'
  end

  put :update, :map => "/sub_categories/:id/update" do
    if @sub_category.update(params[:sub_category])
      success_redirect(url(:sub_categories, :index), 'models.sub_category.success.update')
    else
      render 'sub_categories/edit'
    end
  end

  delete :destroy, :map => "/sub_categories/:id" do
    @sub_category.destroy
    success_redirect(url(:sub_categories, :index), 'models.sub_category.success.destroy')
  end
end
