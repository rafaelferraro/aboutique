module ABoutique
  class Admin < Padrino::Application
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions
  end
end
