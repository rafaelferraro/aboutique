# Helper methods defined here can be accessed in any controller or view in the application

module ABoutique
  class Admin
    module CategoryHelper
      def success_redirect(url, path_to_message)
        flash[:notice] = I18n.t(path_to_message)
        redirect url
      end
    end

    helpers CategoryHelper
  end
end
