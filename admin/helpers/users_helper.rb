# Helper methods defined here can be accessed in any controller or view in the application

module ABoutique
  class Admin
    module UsersHelper
      include ABoutique::Admin::AuthenticationHelper
    end

    helpers UsersHelper
  end
end
