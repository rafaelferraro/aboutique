module ABoutique
  class Admin
    module AdminHelper
      include ABoutique::Admin::AuthenticationHelper
    end

    helpers AdminHelper
  end
end
