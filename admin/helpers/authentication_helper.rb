module ABoutique
  class Admin
    module AuthenticationHelper

      # if anyone is logged, redirects the user to login page
      def resources_authentication
        redirect(url_for(:users, :login)) unless authenticated?
      end

      # verify if a user is logged and
      # if your session and cookie are valid
      def authenticated?
        has_session_and_cookie? && session_is_cookie?
      end

      # create a session and a cookie for 
      # when a user make a sign in
      def create_session_and_cookie
        token = Digest::MD5.new

        session['logged'] = token.to_s
        response.set_cookie('logged', session['logged'])
      end

      # when a user logoff
      # removes the session and the cookie 
      # generated on login 
      def destroy_session
        if has_session_and_cookie? 
          session['logged'] = nil
          response.delete_cookie('logged')
        end
      end

      # verify if request is from login page or not
      def from_login?
        referer = request.env['HTTP_REFERER']
        referer.match('admin/users/login') if referer
      end

      private

      def has_session_and_cookie?
        session['logged'] && request.cookies['logged']
      end

      def session_is_cookie?
        session['logged'] == request.cookies['logged']
      end
    end
  end
end