# Helper methods defined here can be accessed in any controller or view in the application

module ABoutique
  class Admin
    module SubCategoryHelper
      def success_redirect(url, message)
        flash[:notice] = I18n.t(message)
        redirect url
      end
    end

    helpers SubCategoryHelper
  end
end
